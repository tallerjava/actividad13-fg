
import org.junit.Test;
import static org.junit.Assert.*;

public class ReporteCotizacionTest {

    public ReporteCotizacionTest() {
    }

    @Test
    public void getNombreProveedor_ValoresValidosObjetoReporteCotizacion_nombreProveedorObtenido() {
        String resultadoEsperado = "CoinDesk";
        Cotizacion cotizacion = new Cotizacion("18-7-2018 17:23", "USD", 6500.09);
        ReporteCotizacion reporteCotizacion = new ReporteCotizacion(resultadoEsperado, cotizacion);
        String resultadoObtenido = reporteCotizacion.getNombreProveedor();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getCotizacion_ValoresValidosObjetoReporteCotizacion_CotizacionObtenida() {
        Cotizacion cotizacionResultadoEsperado = new Cotizacion("18-7-2018 17:23", "USD", 100.09);
        ReporteCotizacion reporteCotizacion = new ReporteCotizacion("CryptoCompare", cotizacionResultadoEsperado);
        Cotizacion cotizacionResultadoObtenido = reporteCotizacion.getCotizacion();
        assertEquals(cotizacionResultadoEsperado, cotizacionResultadoObtenido);
    }

    @Test
    public void toString_ValoresValidosObjetoReporteCotizacion_StringObtenido() {
        String resultadoEsperado = "CoinDesk: 18-7-2018 17:23 / USD 100,01";
        Cotizacion cotizacion = new Cotizacion("18-7-2018 17:23", "USD", 100.009f);
        ReporteCotizacion reporteCotizacion = new ReporteCotizacion("CoinDesk", cotizacion);
        String resultadoObtenido = reporteCotizacion.toString();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
