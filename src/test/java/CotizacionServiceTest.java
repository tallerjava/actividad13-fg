
//import java.net.ConnectException;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {

    public CotizacionServiceTest() {
    }

    @Test(expected = NullPointerException.class)
    public void obtenerReporteCotizacion_ArgumentoArrayCotizacionRepositoryNull_NullPointerException() {
        ArrayList<CotizacionRepository> listadoCotizacion = null;
        CotizacionService cotizacionService = new CotizacionService(listadoCotizacion);
        cotizacionService.obtenerReporteCotizacion();
    }

    @Test
    public void obtenerReporteCotizacion_ArgumentoValidoArrayCotizacionRepository_reporteObtenido() {
        ArrayList<CotizacionRepository> listadoCotizacion = new ArrayList();
        listadoCotizacion.add(new CoinDeskCotizacionRepository());
        CotizacionService cotizacionService = new CotizacionService(listadoCotizacion);
        assertNotNull(cotizacionService.obtenerReporteCotizacion());
    }
}
