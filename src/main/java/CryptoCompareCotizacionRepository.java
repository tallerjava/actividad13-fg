
import java.text.SimpleDateFormat;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class CryptoCompareCotizacionRepository extends CotizacionRepository {

    private String url = "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD";
    private String nombreProveedor = "CryptoCompare";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject body = new JSONObject(response.body());
        String fecha = new SimpleDateFormat("d-M-yyyy HH:mm").format(new Date());
        String moneda = "USD";
        double precio = body.getDouble("USD");
        Cotizacion cotizacion = new Cotizacion(fecha, moneda, precio);
        return cotizacion;
    }
}
