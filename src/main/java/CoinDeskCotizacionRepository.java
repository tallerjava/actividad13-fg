
import java.text.SimpleDateFormat;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class CoinDeskCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.coindesk.com/v1/bpi/currentprice.json";
    private String nombreProveedor = "CoinDesk";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject body = new JSONObject(response.body());
        String fecha = new SimpleDateFormat("d-M-yyyy HH:mm").format(new Date());
        String moneda = body.getJSONObject("bpi").getJSONObject("USD").getString("code");
        double precio = body.getJSONObject("bpi").getJSONObject("USD").getDouble("rate_float");
        Cotizacion cotizacion = new Cotizacion(fecha, moneda, precio);
        return cotizacion;
    }
}
