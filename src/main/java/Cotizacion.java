
import java.text.DecimalFormat;

public class Cotizacion {

    String fecha, moneda;
    double precio;

    public Cotizacion(String fecha, String moneda, double precio) {
        this.fecha = fecha;
        this.moneda = moneda;
        this.precio = precio;
    }

    public String getFecha() {
        return fecha;
    }

    public String getMoneda() {
        return moneda;
    }

    public double getPrecio() {
        return precio;
    }

    @Override
    public String toString() {
        DecimalFormat formatoDecimal = new DecimalFormat(".##");
        return fecha + " / " + moneda + " " + formatoDecimal.format(precio);
    }
}
