
import java.util.ArrayList;

public class CotizacionService {

    private ArrayList<CotizacionRepository> cotizacionServiceListado;

    public CotizacionService(ArrayList<CotizacionRepository> cotizacionServiceListado) {
        this.cotizacionServiceListado = cotizacionServiceListado;
    }

    public ArrayList<ReporteCotizacion> obtenerReporteCotizacion() {
        ArrayList<ReporteCotizacion> reporteCotizacionListado = new ArrayList();
        for (CotizacionRepository cotizacionRepository : cotizacionServiceListado) {
            String nombreProveedor = cotizacionRepository.getNombreProveedor();
            try {
                Cotizacion cotizacion = cotizacionRepository.obtenerCotizacion();
                reporteCotizacionListado.add(new ReporteCotizacion(nombreProveedor, cotizacion));
            } catch (Exception e) {
                System.out.println(nombreProveedor + ": (el proveedor no se encuentra disponible)");
            }
        }
        return reporteCotizacionListado;
    }
}
